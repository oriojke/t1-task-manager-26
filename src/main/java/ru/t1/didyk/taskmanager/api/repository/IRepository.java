package ru.t1.didyk.taskmanager.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    void clear();

    @NotNull
    List<M> findAll();

    @NotNull
    List<M> findAll(@Nullable Comparator<M> comparator);

    @Nullable
    M add(@Nullable M model);

    boolean existsById(@NotNull String id);

    @Nullable
    M findOneById(@NotNull String id);

    @Nullable
    M findOneByIndex(@Nullable Integer index);

    int getSize();

    @Nullable
    M remove(@Nullable M model);

    @Nullable
    M removeById(@NotNull String id);

    @Nullable
    M removeByIndex(@NotNull Integer index);

    void removeAll(@Nullable List<M> modelsRemove);

}
