package ru.t1.didyk.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.enumerated.Sort;
import ru.t1.didyk.taskmanager.model.Task;
import ru.t1.didyk.taskmanager.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskListCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Display all tasks.";
    }

    @NotNull
    @Override
    public String getName() {
        return "task-list";
    }

    @Override
    public void execute() {
        @Nullable final String userId = getUserId();
        System.out.println("[SHOW TASKS]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @Nullable final Sort sort = Sort.toSort(sortType);
        @Nullable final List<Task> tasks = getTaskService().findAll(userId, sort);
        renderTasks(tasks);
    }

}
