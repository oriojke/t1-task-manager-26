package ru.t1.didyk.taskmanager.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.didyk.taskmanager.api.service.IPropertyService;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @NotNull
    @Override
    public String getArgument() {
        return "-a";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show developer info.";
    }

    @NotNull
    @Override
    public String getName() {
        return "about";
    }

    @Override
    public void execute() {
        @NotNull final IPropertyService propertyService = getPropertyService();
        System.out.println("[ABOUT]");
        System.out.println("Author: " + propertyService.getAuthorName());
        System.out.println("email: " + propertyService.getAuthorEmail());
        System.out.println();
        System.out.println("[GIT]");
        System.out.println("Commit ID: " + propertyService.getCommitId());
        System.out.println("Branch: " + propertyService.getGitBranch());
        System.out.println("Commit message: " + propertyService.getGitCommitMessage());
        System.out.println("Committer name: " + propertyService.getCommitterName());
        System.out.println("Committer email: " + propertyService.getCommitterEmail());
        System.out.println("Commit time: " + propertyService.getGitCommitTime());
    }
}
